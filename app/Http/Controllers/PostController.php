<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PostController extends Controller
{
    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $query = DB::table('pertanyaan')->insert(
            [
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);
        return redirect('/posts')->with('success', 'Post Berhasil Disimpan!');
    }

    public function index(){
        $posts = DB::table('pertanyaan')->get();
        //dd($posts);
        return view('posts.index', compact('posts'));
    }

    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('posts.show', compact('post'));
    }
    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('posts.edit', compact('post'));
    }
    public function update($id, Request $request){
        $query = DB::table('pertanyaan')->where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/posts')->with('success', 'Berhasil update post!');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/posts')->with('success', 'Berhasil dihapus!');
    }
}

