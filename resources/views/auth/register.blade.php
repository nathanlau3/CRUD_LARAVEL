<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <form action="/welcome" method="POST">
    @csrf
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    
        <label for="first_name">First name:</label>
        <br>
        <br>
        <input type="text" id="first_name" name="depan">
        <br> 
        <br>
        <label for="last_name">Last name:</label>
        <br>
        <br>
        <input type="text" id="last_name" name="belakang">
        <br>
        <br>
        <label for="gender">Gender:</label>
        <br>
        <br>
        <input type="radio" name="gender" id="Male"><label for="Male">Male</label> <br>
        <input type="radio" name="gender" id="Female"><label for="Female">Female</label> <br>
        <input type="radio" name="gender" id="Other"><label for="Other">Other</label> <br>
        <p>Nationality:</p>
        <select name="nationality" id="">
            <option value="Indonesia">Indonesian</option>
            <option value="Malaysia">Malaysian</option>
            <option value="Australia">Australian</option>
            <option value="Singapore">Singapore</option>
        </select>
        <br>
        <br> 
        <label for="language">Language Spoken:</label> <br> <br>
        <input type="checkbox" id="Bahasa Indonesia"><label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English"><label for="English">English</label><br>
        <input type="checkbox" id="Other"><label for="Other">Other</label><br>
        <br>
        <p>Bio:</p>
        <textarea name="" id="bio" cols="30" rows="10"></textarea>
        <br>
        <button>Sign Up</button>
        </form>
</body>
</html>