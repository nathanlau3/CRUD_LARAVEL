@extends('master')

@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">Tabel Post</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <a class="btn btn-primary mb-3" href="/posts/create">Create Post</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul Pertanyaan</th>
            <th>Isi pertanyaan</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($posts as $key=>$post)
              <tr>
                  <td>
                      {{$key + 1}}
                  </td>
                  <td>
                      {{$post->judul}}
                  </td>
                  <td>
                      {{$post->isi}}
                  </td>
                  <td style="display: flex">
                      <a href="/posts/{{$post->id}}" class="btn btn-info btn-sm">Show</a>
                      <a href="/posts/{{$post->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                      <form action="/posts/{{$post->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger">
                    </form>
                  </td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div> --}}
  </div>
    
@endsection