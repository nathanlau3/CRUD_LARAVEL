@extends('master')

@section('content')
<div class="ml-3 mt-3">

    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Post</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/posts" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Title</label>
              <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul">
            </div>
            <div class="form-group">
              <label for="isi">Body</label>
              <input type="text" class="form-control" name="isi" id="judul" placeholder="Isi">
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
        
</div>
@endsection