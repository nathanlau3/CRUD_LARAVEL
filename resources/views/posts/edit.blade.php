@extends('master')

@section('content')
<div class="ml-3 mt-3">

    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Post {{$post->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/posts/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Title</label>
              <input type="text" class="form-control" name="judul" value="{{old('judul', $post->judul)}}" id="judul" placeholder="Judul">
            </div>
            <div class="form-group">
              <label for="isi">Body</label>
              <input type="text" class="form-control" name="isi" value="{{old('judul', $post->judul)}}" id="judul" placeholder="Isi">
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
        
</div>
@endsection